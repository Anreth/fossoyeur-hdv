
import json

def write_json(fileName, jsonVar):
    json_string = json.dumps(jsonVar, indent=4)
    with open(fileName + ".json", 'w') as outfile:
        outfile.write(json_string)


def test_blizzard():
    from blizzardapi import BlizzardApi
    try :
        from eheh import client_id, client_secret
    except Exception:
        print("######### Erreur : Il faut créer le fichier eheh.py et y ajouter les deux variables de clés client_id et client_secret")
    

    api_client = BlizzardApi(client_id, client_secret)
    del client_id, client_secret

    region = "eu"
    locale = "fr_FR"
    connected_realm_id = 4441 # Auberdine
    auction_house_id = 6 # Horde

    # test = api_client.wow.game_data.get_auctions_for_auction_house(region, locale, connected_realm_id, auction_house_id)
    test1 = api_client.wow.game_data.get_item(region, locale, 23571, is_classic=True)
    test2 = api_client.wow.game_data.get_professions_index(region, locale)
    test3 = api_client.wow.game_data.get_profession(region, locale, 171)
    test4 = api_client.wow.game_data.get_profession_skill_tier(region, locale,171,2484)
    test5 = api_client.wow.game_data.get_recipe(region, locale, 14157)
    
    write_json("./dbEx/blizz_get_item_23571",test1)
    write_json("./dbEx/blizz_get_professions_index",test2)
    write_json("./dbEx/blizz_get_profession alchi",test3)
    write_json("./dbEx/blizz_get_profession_skill_tier alchi OT",test4)
    write_json("./dbEx/blizz_get_recipe alchi unk",test5)
    

def test_nexus():
    import requests
    
    "https://api.nexushub.co/wow-classic/v1/crafting/auberdine-horde/2841" 
    # r = requests.get('https://httpbin.org/basic-auth/user/pass', auth=('user', 'pass'))
    
    # craft barre de bronze
    r = json.loads(requests.get("https://api.nexushub.co/wow-classic/v1/crafting/auberdine-horde/2841").text)
    write_json("./dbEx/nexus_crafting_barreBron",r)


if __name__ == '__main__':
    
    test_blizzard()
    test_nexus()
    print("otot")