


from xml.dom import NoModificationAllowedErr
import luadata
import re
from TMP import lolData


# objectList
#     list object
#             Nom
#             id
#             class auctionsList
#             def getPriceForXItem
            
#             class craftRecipe
#                 (nbobject, objectid)
#             def getCraftPriceForXItem




        
        
        
class ObjectWoWList:
    def __init__(self):
        self.list = dict()
        
    def append(self, object):
        self.list[object.getID()] = object
    
    def getObject(self, id):
        if id in self.list :
            return self.list[id]
    
        return ObjectWoW(0)
        

class ObjectWoW:
    def __init__(self, id):
        self.id = id
        self.name = ""

    
    def getID(self):
        return self.id
        
    def addAuctionList(self, acList):
        self.acList = acList
    
    def addName(self, dictName):
        
        if self.id in dictName:
            self.name = dictName[self.id]
            
    def getPriceForXItem(self, nbRequireItem):
        return self.acList.getPriceForXItem(nbRequireItem)
            
    def __repr__(self):
        return self.name + " (" + self.id + ") -> " + str(self.acList)
    
    def addCraft(self, craft):
        self.craft = craft
        
    def getCraftPrice(self, nb):
        return self.craft.getCraftPriceForXCraftPerUnitCrafted(nb)
    
    def getDetailledCraftPrice(self, nb):
        return self.craft.getDetailledCraftPrice(nb)

class AuctionList:
    def __init__(self):
        self.aList = []
        
    def add(self, ac):
        self.aList.append(ac)

    def getPriceForXItem(self, nbRequireItem):
        self.aList.sort()
        
        totalItem = 0
        totalPrice = 0
        done = False
        for ac in self.aList:
            (price, nbItem) = ac.getPriceInfo()
            if totalItem + nbItem >= nbRequireItem:
                
                overShout = totalItem + nbItem - nbRequireItem
                totalItem = nbRequireItem
                totalPrice += price*(nbItem-overShout)
                done = True
                break
            else:
                totalItem += nbItem
                totalPrice += price*nbItem
        
        if done :
            return totalPrice
        else:
            return "NA"

    def __str__(self):
        
        price = self.getPriceForXItem(5)
        if isinstance(price, int):
            return "Unit price of 5 item : " + str(GAC(int(price/5)+1))
        else:
            return "Moins de 5 items en vente"

class GAC:
    def __init__(self, copper):
        if isinstance(copper, int):
            self.gold = int(copper / 100 / 100)
            self.silver = int(copper / 100 ) - self.gold*100
            self.copper = copper  - self.silver*100- self.gold*100*100
       
    def __str__(self):
        return str(self.gold) + "g " + str(self.silver) + "s " + str(self.copper) + "c"
    


class Auction:
    def __init__(self, list):
        self.time = list[0]
        self.nbItem = int(list[1])
        self.bid = int(list[2])
        if list[3] == "":
            self.buyout = 999999999999999
        else:
            self.buyout = int(list[3])
        self.curBid = list[4]
    
    def getPriceInfo(self):
        return (self.buyout,self.nbItem)
    
    def __lt__(self, other):
        return self.buyout < other.buyout
    
    def __repr__(self):
        return str(GAC(self.buyout))
        

objetList = ObjectWoWList()


class CraftRecipe:
    def __init__(self, outputQt):
        self.outputQt = outputQt
        self.componentList = {}

    def addComponent(self, id, nb):
        self.componentList[id] = nb
        
    def getCraftPriceForXCraftPerUnitCrafted(self, X):
        totalPrice = 0
        for (componentID, qt) in self.componentList.items():
            
            cObjectPrice = objetList.getObject(componentID).getPriceForXItem(X*qt)
            if not isinstance(cObjectPrice, int):
                return "NA"
            
            totalPrice += cObjectPrice
            
        return int(totalPrice/self.outputQt)+1

    def getDetailledCraftPrice(self, X):
        totalPrice = 0
        finalStr = ""
        for (componentID, qt) in self.componentList.items():
            
            cObjectTotalPrice = objetList.getObject(componentID).getPriceForXItem(X*qt)
            if not isinstance(cObjectTotalPrice, int):
                return "Non calculable"
            cObjectUnitPrice = int(objetList.getObject(componentID).getPriceForXItem(X*qt)/qt)+1
            
            finalStr += "\t" + str(qt) + "unité de " + componentID + " prix/u :" + str(GAC(cObjectUnitPrice)) + " -> prix total : " + str(GAC(cObjectTotalPrice)) + "\n"

            totalPrice += cObjectTotalPrice
            
        finalStr += " ***** Pour un prix total de : " + str(GAC(totalPrice))
        return finalStr


if __name__ == '__main__':
    FILE_PATH = "F:/Wow Classic/World of Warcraft/_classic_/WTF/Account/404592980#1/SavedVariables"
    FILE_NAME = "AuctionDB_TMP2.lua"
    COMPLETE_PATH = FILE_PATH + "/" + FILE_NAME

    IGNORE_STUFF_VARIATION = True 

    objectName_dict = {}

    data = luadata.read(COMPLETE_PATH, encoding="utf-8")
    del FILE_PATH, FILE_NAME, COMPLETE_PATH
    # id and name from lua
    itemDB = data["itemDB_11"]
    
    # create simplified id and name dict
    for objet in itemDB:
               
        # ignore formatVersion/created.. extract
        if not objet.startswith('_') and not objet.endswith('_'):
            # Extract name at string ending between []
            object_name = re.search('\[(.*)\]', itemDB[objet]).group(1)
            objectName_dict[objet] = object_name
    del itemDB, object_name, objet

    # # extract auction data
    # auctionDB = data["ah"][0]["data"].split(' ')
    
    auctionDB = lolData.split(' ')
    

    for objectAuction in auctionDB:
        
        if len(objectAuction) > 5:
            objectId = re.search('(i\d+[\?\-\:]{0,2}\d+)\!', objectAuction).group(1)
            if IGNORE_STUFF_VARIATION and ("?" in objectId or "-" in objectId or ":" in objectId):
                continue
                
            cObject = ObjectWoW(objectId)
            cObject.addName(objectName_dict)
            
            objetList.append(cObject)            
            
            
            auctionList = re.search('\!(.*)', objectAuction).group(1).replace("/","&").lstrip("&").split('&')

            acList = AuctionList()
            for auction in auctionList:
                ac = Auction(auction.split(","))
                acList.add(ac)
                
            cObject.addAuctionList(acList)
            print(cObject)

    
    testCraft = CraftRecipe(1)
    testCraft.addComponent("i43332",2)
    testCraft.addComponent("i7078",2)
    objedsq = objetList.getObject("i23105")
    
    objedsq.addCraft(testCraft)
    
    
    
    test1 = objetList.getObject("i43332").getPriceForXItem(1)
    test2 = objetList.getObject("i7078").getPriceForXItem(1)
    test = objedsq.getCraftPrice(1)
    print(objedsq.getDetailledCraftPrice(1))
    
        
    
    
    
    print("ttot")




