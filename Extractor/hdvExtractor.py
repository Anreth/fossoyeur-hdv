import json
from blizzardapi import BlizzardApi
try :
    from eheh import client_id, client_secret
except Exception:
    print("######### Erreur : Il faut créer le fichier eheh.py et y ajouter les deux variables de clés client_id et client_secret")
    
region = "eu"
locale = "fr_FR"
connected_realm_id = 4441 # Auberdine
auction_house_id = 6 # Horde
api_client = BlizzardApi(client_id, client_secret)

def buyoutSort(var):
    return var["buyout"]

def write_json(fileName, jsonVar):
    json_string = json.dumps(jsonVar, indent=4)
    with open(fileName + ".json", 'w') as outfile:
        outfile.write(json_string)

def test():
    # test1 = api_client.wow.game_data.get_item(region, locale, item_id, is_classic=True)
    auction_list = api_client.wow.game_data.get_auctions_for_auction_house(region, locale, connected_realm_id, auction_house_id)['auctions']
    auction_dict = {}
    
    
    # tri des auctions sous forme dict[idItem] = [listeAuction]
    cpt = 1
    for auction in auction_list:
        
        percent = str(int(cpt / len(auction_list)*1000) / 10)
        print("Traitement : " + str(cpt) + "/" + str(len(auction_list)) + "  -->  " + percent + "%")
        cpt = cpt + 1
        
        itemID = auction['item']['id'] 
        if 'rand' not in auction['item']: # rand = stuff de la chouette
            if itemID not in auction_dict:
                auction_dict[itemID] = {}
                # auction_dict[itemID]["name"] = api_client.wow.game_data.get_item(region, locale, itemID, is_classic=True)['name']
                auction_dict[itemID]["auction"] = []
                
            a_dict = {}
            a_dict["bid"] = auction["bid"]
            a_dict["buyout"] = auction["buyout"]
            a_dict["quantity"] = auction["quantity"]
            a_dict["time_left"] =auction["time_left"]
            auction_dict[itemID]["auction"].append(a_dict)
            
    for itemID in auction_dict:
        auction_dict[itemID]["auction"].sort(key=buyoutSort)
        
    # write_json("./../dbEx/exHdv",auction_dict)

    






if __name__ == '__main__':
    api_client = BlizzardApi(client_id, client_secret)
    test()
    
    
    
    