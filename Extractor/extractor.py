import json
import requests
from blizzardapi import BlizzardApi
try :
    from eheh import client_id, client_secret
except Exception:
    print("######### Erreur : Il faut créer le fichier eheh.py et y ajouter les deux variables de clés client_id et client_secret")
    
region = "eu"
locale = "fr_FR"
connected_realm_id = 4441 # Auberdine
auction_house_id = 6 # Horde
api_client = BlizzardApi(client_id, client_secret)

def extract_profession():
    profession_list = []
    prof_index = api_client.wow.game_data.get_professions_index(region, locale)
    for prof in prof_index["professions"]:
        
        if prof["id"] < 773: # ignore des métiers cata+
            prof_dict = {}
            prof_dict["name"] = prof["name"]
            prof_dict["id"] = prof["id"]

            profession_list.append(prof_dict)

    
    return profession_list

def extract_profession_tier(profession_list):
    
    for prof in profession_list:
        skill_tiers = api_client.wow.game_data.get_profession(region, locale, prof["id"])
        enhance_profession_list = []

        for tmp in skill_tiers["skill_tiers"]:
            if "Outreterre" in tmp["name"] or "Norfendre" in tmp["name"] or tmp["name"].count(" ") < 1 :
                # bug tdc !!
                skill_tmp = {}
                skill_tmp["id"] = tmp["id"]
                skill_tmp["name"] = tmp["name"]
                enhance_profession_list.append(skill_tmp)
                
        prof["SkillTierList"] = enhance_profession_list

    return profession_list
    
def extract_recipe_profession(profession_list):
    
    for prof in profession_list:
        id_prof = prof["id"]
        for skilltier in prof["SkillTierList"]:
            id_skill_tier = skilltier["id"]
            
            sktier = api_client.wow.game_data.get_profession_skill_tier(region, locale,id_prof,id_skill_tier)
            
            skilltier["recipe"] = {}
            if "categories" not in sktier:
                continue
            for categorie in sktier["categories"]:
                name_cat = categorie["name"]
                recipe_list = []
                skilltier["recipe"][name_cat] = recipe_list
                for recipe in categorie["recipes"]:
                    recipe_dict = {}
                    recipe_dict["name"] = recipe["name"]
                    recipe_dict["id"] = recipe["id"]
                    recipe_list.append(recipe_dict)
                    
    return profession_list     
                    
                
def extract_detailled_recipe(profession_list):
    for prof in profession_list:
        for skilltier in prof["SkillTierList"]:
            for categorie in skilltier["recipe"]:
                print(prof["name"] + "/" + skilltier["name"] + "/" + categorie)
                tmp_list = []
                for recipe in skilltier["recipe"][categorie]:
                    r = api_client.wow.game_data.get_recipe(region, locale, str(recipe["id"]))
                    tmp_r = {}
                    tmp_r["id"] = r["id"]
                    tmp_r["name"] = r["name"]
                    tmp_r["crafted_item"] = {}
                    if "crafted_item" in r:
                        tmp_r["crafted_item"]["name"] = r["crafted_item"]["name"]
                        tmp_r["crafted_item"]["id"] = r["crafted_item"]["id"]
                    if "reagents" in r:
                        tmp_r["reagents"] = r["reagents"]
                    if "crafted_quantity" in r:
                        tmp_r["crafted_quantity"] = r["crafted_quantity"]
                    tmp_list.append(tmp_r)
                skilltier["recipe"][categorie] = tmp_list
                
                    
    
    
    return profession_list




def write_json(fileName, jsonVar):
    json_string = json.dumps(jsonVar, indent=4)
    with open(fileName + ".json", 'w') as outfile:
        outfile.write(json_string)
    
if __name__ == '__main__':
    api_client = BlizzardApi(client_id, client_secret)

    
    
    profession_list = extract_profession()
    profession_list = extract_profession_tier(profession_list)
    profession_list = extract_recipe_profession(profession_list)
    profession_list = extract_detailled_recipe(profession_list)
    
    write_json("./../dbEx/prof",profession_list)
    print("toto")
    

    
        
    
    
    
    




